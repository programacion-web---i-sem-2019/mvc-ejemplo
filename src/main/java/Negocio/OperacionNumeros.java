/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.ConjuntoNumeros;

/**
 *
 * @author MADARME
 */
public class OperacionNumeros {
    
    private ConjuntoNumeros myNumeros;

    public OperacionNumeros() {
    }

    public OperacionNumeros(ConjuntoNumeros myNumeros) {
        this.myNumeros = myNumeros;
    }

    public ConjuntoNumeros getMyNumeros() {
        return myNumeros;
    }

    public void setMyNumeros(ConjuntoNumeros myNumeros) {
        this.myNumeros = myNumeros;
    }
    
    
    public int getSumar()
    {
        if(this.myNumeros.getNumeros()==null)
            return 0;
        
        int s=0;
        int i=0;
        for(int dato:this.myNumeros.getNumeros())
            s+=this.myNumeros.getNumeros()[i++];
        
        return s;
            
        
    }
    
    
}
