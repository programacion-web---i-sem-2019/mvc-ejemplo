/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

import Modelo.ConjuntoNumeros;
import Negocio.OperacionNumeros;

/**
 *
 * @author MADARME
 */
public class Facade {
    
    
    private OperacionNumeros operacion;
    //las demás clases del negocio

    public Facade() {
    }
    
    
    public void crearConjuntoNumeros(String datos) throws Exception
    {
    
        ConjuntoNumeros n=new ConjuntoNumeros(datos);
        this.operacion=new OperacionNumeros(n);
    
    }
    
    public int getSumar()
    {
        return this.operacion.getSumar();
    }
    
    
}
