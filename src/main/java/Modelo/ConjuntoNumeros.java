/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author MADARME
 */
public class ConjuntoNumeros {
    
    private int numeros[];

    public ConjuntoNumeros() {
    }
    
    
    public ConjuntoNumeros(String numeros) throws Exception {
        
        String datos[]=numeros.split(",");
        this.numeros=new int[datos.length];
        int i=0;
        for(String dato: datos)
            try{
                this.numeros[i++]=Integer.parseInt(dato);
            }catch(NumberFormatException e)
            {
                //Runtime --> no es necesario try-catch
            throw new Exception("mal formateado los datos, existe un dato que no es númerico");
            }
        
    }

    public int[] getNumeros() {
        return numeros;
    }

    public void setNumeros(int[] numeros) {
        this.numeros = numeros;
    }
    
    
    
}
